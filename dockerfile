FROM maven:latest
WORKDIR /home/data
# CMD mvn -f /home/data/pom.xml clean package && sleep infinity

CMD mvn -f /home/data/pom.xml clean package && \
java -jar target/microblog-web-application-1.0-SNAPSHOT.jar