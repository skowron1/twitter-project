# twitter-project
Projekt zespołowy utworzenia aplikacji webowej przypominającej funkcjonaloność twittera

# usage docker
docker compose build --no-cache
docker compose up -d
docker compose logs
docker exec -it skowronki-java-1 bash

# usage hsqldb sqltool
Available environment variables:

JAVA_VM_PARAMETERS (default: "-Dfile.encoding=UTF-8")
<br>
HSQLDB_USER (default: "sa")
<br>
HSQLDB_PASSWORD (default: ""(empty password))
<br>
HSQLDB_TRACE (default: "-trace true")
<br>
HSQLDB_SILENT (default: "-silent false")
<br>
HSQLDB_REMOTE (default: "-remote_open true")
<br>
HSQLDB_DATABASE_NAME (default: "hsqldb")
<br>
HSQLDB_DATABASE_ALIAS (default: "test") current skowronki
<br>
HSQLDB_DATABASE_HOST (default: "localhost")

Current Connection Url: jdbc:hsqldb:hsql://localhost:9001/skowronki