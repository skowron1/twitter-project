package org.wwsis.dao;

import org.wwsis.model.Post;

import java.util.List;

public interface PostDao {
    List<Post> findAllPostsByUserId(Long userId);
    List<Post> findAllPostByUserIdsAndFollowersIds(List<Long> ids);
    List<Post> findAllPosts();
}