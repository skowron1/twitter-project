package org.wwsis.dao;

import org.wwsis.model.Follower;

public interface FollowerDao {

    void saveFollower(Follower follower);
    void deleteFollower(Follower follower);
}
