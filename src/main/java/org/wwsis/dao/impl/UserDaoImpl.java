package org.wwsis.dao.impl;

import org.wwsis.dao.UserDao;
import org.wwsis.model.User;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;

@Transactional
public class UserDaoImpl implements UserDao {

    @PersistenceContext
    EntityManager entityManager;

    @Override
    public User findUserByEmail(String email) {
        String select = "Select u FROM User u WHERE u.email = :email";
        Query query = entityManager.createQuery(select);
        query.setParameter("email", email);
        return (User) query.getSingleResult();
    }

    @Override
    public void saveUser(User user) {
        entityManager.persist(user);
    }
}
