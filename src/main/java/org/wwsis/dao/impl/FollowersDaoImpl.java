package org.wwsis.dao.impl;

import org.wwsis.dao.FollowerDao;
import org.wwsis.model.Follower;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

@Transactional
public class FollowersDaoImpl implements FollowerDao {

    @PersistenceContext
    EntityManager entityManager;

    @Override
    public void saveFollower(Follower follower) {
        entityManager.persist(follower);
    }

    @Override
    public void deleteFollower(Follower follower) {
        entityManager.remove(follower);
    }
}
