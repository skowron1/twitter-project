package org.wwsis.dao.impl;

import org.wwsis.dao.PostDao;
import org.wwsis.model.Post;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import javax.transaction.Transactional;
import java.util.List;

@Transactional
public class PostDaoImpl implements PostDao {
    @PersistenceContext
    EntityManager entityManager;

    @Override
    public List<Post> findAllPostsByUserId(Long userId) {
        String select = "SELECT p FROM Post p WHERE p.users = :userId";
        Query query = entityManager.createQuery(select);
        query.setParameter("userId", userId);
        return (List<Post>) query.getResultList();
    }

    @Override
    public List<Post> findAllPostByUserIdsAndFollowersIds(List<Long> ids) {
        String select = "SELECT p FROM Post p WHERE p.users in (:ids)";
        Query query = entityManager.createQuery(select);
        query.setParameter("ids", ids);
        return (List<Post>) query.getResultList();
    }

    @Override
    public List<Post> findAllPosts() {
        String select = "SELECT p FROM Post p ";
        Query query = entityManager.createQuery(select);
        return (List<Post>) query.getResultList();
    }
}