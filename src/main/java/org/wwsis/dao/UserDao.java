package org.wwsis.dao;

import org.wwsis.model.User;

public interface UserDao {

    User findUserByEmail(String email);
    void saveUser(User user);
}
