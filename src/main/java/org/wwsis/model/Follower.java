package org.wwsis.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "followers")
public class Follower {

    @Column(name = "id_follower", nullable = false)
    private Long followerId;

    @Column(name = "id_user", nullable = false)
    private Long userId;

    public Long getFollowerId() {
        return followerId;
    }

    public void setFollowerId(Long followerId) {
        this.followerId = followerId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }
}
