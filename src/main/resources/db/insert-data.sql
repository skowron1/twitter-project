INSERT INTO users (id_user,email,nick,password) VALUES 
(1,'s6938@horyzont.eu','Tomek','123Frytki'),
(2,'s6929@horyzont.eu','Emil','123Kebab'),
(3,'s6825@horyzont.eu','Iwona','123Pizza'),
(4,'s6928@horyzont.eu','Michał','123HotDog'),
(5,'profesor@horyzont.eu','Profesor','123Hamburger');

INSERT INTO followers (id_follower,id_user) VALUES
(5,1),
(5,2),
(5,3),
(5,4);

INSERT INTO posts (id_user,title,content,likes) VALUES
(3, 'Jakie fast-foody lubimy?', '
<h1>Jakie fast-foody lubimy?</h1>
<ul>
<li>Zdrowsze?</li>
<li>Smaczniejsze?</li>
<li>Tłuste?</li>
</ul>
', 5),
(2, 'Co robimy po zajęciach?', '
<h1>Co robimy po zajęciach?</h1>
<ul>
<li>Śpimy?</li>
<li>Ciśniemy?</li>
<li>Robimy zakupy?</li>
</ul>
', 3),
(4, 'Jakie sporty uprawiamy?', '
<h1>Jakie sporty uprawiamy?</h1>
<ul>
<li>Biegamy?</li>
<li>Spływamy?</li>
<li>Ćwiczymy siedzenie?</li>
</ul>
', 2),
(1, 'Jak spędzamy wakacje?', '
<h1>Jak spędzamy wakacje?</h1>
<ul>
<li>Ciśniemy dalej?</li>
<li>Jedziemy nad morze?</li>
<li>Zwiedzamy inne kraje?</li>
</ul>
', 3);