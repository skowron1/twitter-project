DROP TABLE IF EXISTS users;
CREATE TABLE users
(
    id_user    IDENTITY PRIMARY KEY,
    email      VARCHAR(120) NOT NULL,
    nick       VARCHAR(100) NOT NULL,
    password   VARCHAR(255) NOT NULL,
    last_login DATETIME DEFAULT now(),
    UNIQUE (email)
);

DROP TABLE IF EXISTS followers;
CREATE TABLE followers
(
    id_follower INT NOT NULL,
    id_user     INT NOT NULL,
    FOREIGN KEY (id_follower) REFERENCES users (id_user),
    FOREIGN KEY (id_user) REFERENCES users (id_user),
    UNIQUE (id_follower, id_user)
);

DROP TABLE IF EXISTS posts;
CREATE TABLE posts
(
    id_post IDENTITY PRIMARY KEY,
    id_user INT NOT NULL,
    title   VARCHAR(150) NOT NULL,
    content VARCHAR(255) NOT NULL,
    likes   INT DEFAULT 0,
    FOREIGN KEY (id_user) REFERENCES users (id_user)
);
